build:
	gcc -o pam_curlftpfs.so -I. pam_module.c -lpam -shared
install:
	install pam_curlftpfs.so $(DESTDIR)/lib/security/
clean:
	rm -f pam_curlftpfs.so
