# pam curlftps
mount home directory using curlftpfs

## Install
1. edit config.h file
2. build source:
```
make
install pam_curlftpfs /lib/security/
```
3. edit /etc/pam.d/xx file and add:
```
auth		optional	pam_curlftsfp.so
```
