#include <config.h>

#include <security/pam_modules.h>
#include <security/pam_appl.h>
#include <pwd.h>

#include <stdio.h>
#include <stdlib.h>

int pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv){
    return PAM_SUCCESS;
}
int pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    char* user;
    pam_get_user(pamh,&user,NULL);
    char* pass;
    pam_get_authtok(pamh,PAM_AUTHTOK,&pass,NULL);
    mount_curlftpfs(user,pass);
    return PAM_SUCCESS;
}

int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return PAM_SUCCESS;
}

int mount_curlftpfs(char *user, char* passwd){
    struct passwd* pwd;
    pwd = getpwnam(user);
    char *cmd;
    int r = asprintf(&cmd,"grep %s/%s/%s /proc/mounts>/dev/null",home, user,subdirectory);
    if(system(cmd)==0){
        return PAM_SUCCESS;
    }
    r = asprintf(&cmd,"mkdir -p %s/%s/%s -m 777",home, user,subdirectory);
    system(cmd);
    r = asprintf(&cmd,"curlftpfs ftp://%s:21 -o user=%s:%s -o nonempty -o uid=%d,allow_other %s/%s/%s", 
                     server,
                     user,
                     passwd,pwd->pw_uid,
                     home,
                     user,
                     subdirectory);
    system(cmd);
    return PAM_SUCCESS;
}
